# ResonantTank [![Build Status](https://travis-ci.org/wapidstyle/ResonantTank.svg?branch=master)](https://travis-ci.org/wapidstyle/ResonantTank)
Tank is a lightweight & open source Minecraft server that supports plugins written for the Bukkit & Spigot API.  

The main goals are to provide a good implementation of the Minecraft server where higher performance is desired
than the official software can deliver.

## Features

Check the [wiki](https://github.com/wapidstyle/CreativeTank/wiki) for more info.
