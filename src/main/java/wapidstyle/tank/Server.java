package wapidstyle.tank;

import java.io.IOException;
import java.net.Socket;
import java.net.SocketAddress;
import java.net.InetSocketAddress;
import java.net.InetAddress;

import wapidstyle.api.CompiledValues;

public class Server {
	
	/** The listener for the server. */
	protected static Socket listener;
	/** The address of the server. */
	protected static SocketAddress address;
	
	/** A list of command-line arguments. Used only by Server and Console, you don't
	 * need to use this for custom command line elements.
	 */
	protected static CompiledValues arguments = new CompiledValues();
	
	/** DO NOT USE!! Used so java can run something. */
	public static void main(String args[]){
		arguments.add(ArgumentListener.convert(args[0], ElementType.BOOLEAN).bool);
		arguments.add(ArgumentListener.convert(args[1], ElementType.BOOLEAN).bool);
		arguments.add(ArgumentListener.convert(args[2], ElementType.INTEGER).integer);
		beginListening();
	}
	/** Begins listening on the port. */
	private static void beginListening(){
		int port = arguments.integers.get(0);
		try {
			listener = new Socket(InetAddress.getLocalHost(), port);
			Console.logDebug("Successfully got local host, creating address...");
			address = new InetSocketAddress(InetAddress.getLocalHost(), port);
			Console.logDebug("Successfully made address, binding...");
			listener.bind(address);
			Console.logDebug("Done binding to " + new Integer(port).toString() + "!");
			Console.logDebug("Step 1 Complete!");
		} catch (IOException e) {
			ErrorHandler.logError("Failed to open socket!", true, -1, e);
		}	
	}
}
