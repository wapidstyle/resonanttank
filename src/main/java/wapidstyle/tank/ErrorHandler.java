package wapidstyle.tank;

import static java.lang.System.out;

import java.io.File;

public class ErrorHandler  {
	public static void logError(String errormsg, boolean sIP, int exit, Throwable e){
		Console.logCustom(errormsg, Console.error, true);
		if(sIP == true){
			Console.logCustom("bit.ly/tankissues", Console.error, true);
		}
		Console.logCustom(e.getMessage(), Console.error, true);
		Console.logError(e.getCause());
		logError(e);
		Console.logCustom("Exiting with exit code " + new Integer(exit).toString(),
				Console.error, true);
		System.exit(exit);
	}
	public static void logError(Throwable e){e.printStackTrace();}
	protected static void logError(File file){
		int retrycount = 0;
		String message = "Can't write to log file, attempting to fix... (" + retrycount + "/5) tries remaining!";
		out.println(message);
		while(file.canWrite() == false){}
		Console.logToFile("Log Fixed!", file, file.getAbsolutePath());
	}
	private ErrorHandler(){}
}
