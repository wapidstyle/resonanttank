package wapidstyle.tank;

import static java.lang.System.out;
import static java.lang.System.err;

import java.io.IOException;
import java.io.FileNotFoundException;
import java.io.File;
import java.io.FileOutputStream;
import java.io.ObjectOutputStream;
import java.time.Clock;
import java.time.LocalDate;

import wapidstyle.api.Boolean;

public class Console {
	
	public static final String error = "[ERROR]";
	public static final String info = "[INFO]";
	public static final String warning = "[WARNING]";
	public static final String debug = "[DEBUG]";
	
	private Console(){}
	
	public void beginLogToFile(String text){
		String filename = getDate() + "-tank.log";
		File file = new File(filename);
		final String path = file.getAbsolutePath();
		if(file.canWrite() == true){logToFile(text, file, path);} else {ErrorHandler.logError(file);}
	}
	public static void clearLog(){
		@SuppressWarnings("unused")
		int i = 5000;
		for(i = 0; ;){
			logRaw(" ");
			i++;
		}
	}
	protected static void logToFile(String text, File file, String path){
		// Create FileOutputStream because ObjectOutputStream requires it unless you use
		// a protected constructor which is unintended.
		FileOutputStream outstream;
		// Create ObjectOutputStream to output text (java.lang.String) to a file
		ObjectOutputStream output;
		try {
			outstream = new FileOutputStream(file);
			output = new ObjectOutputStream(outstream);
			output.writeChars(text);
		} catch (FileNotFoundException e) {
			logError("Could not find log file, try again later!",
					null, e, true);
			System.exit(-1);
		} catch (IOException e) {
			logError("I/O Error!", e.getMessage(), e, true);
			System.exit(-1);
		}
	}
	public static String getDate(){return LocalDate.now().toString();}
	public static String getTime(){return "[" + Clock.systemUTC().toString() + "]";}
	
	public static void log(String text){
		String message = "";
		message = getTime() + "[INFO]" + text;
		out.println(message);
		message = null;
	}
	public static void logRaw(String text){
		out.println(text);
	}
	public static void logCustom(String text, String prefix, boolean showTime){
		String message = "";
		if(showTime == true){
			message = getTime() + prefix + text;
		} else {
			message = message + prefix + text;
		}
	}
	public static void logError(Throwable cause){err.println(cause.toString());}
	public static void logError(String line1, String line2, Throwable e, boolean showTime){
		logCustom(line1, error, showTime);
		logCustom(line2, error, showTime);
		logCustom(e.getMessage(), error, showTime);
		logError(e.getCause());
		e.printStackTrace();
	}
	public static void logDebug(String text){
		if(Server.arguments.booleans.get(0) == Boolean.TRUE){
			out.println(getTime() + debug + text);
		}
	}
}
