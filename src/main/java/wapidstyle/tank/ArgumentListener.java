package wapidstyle.tank;

import wapidstyle.api.Boolean;
import wapidstyle.api.CompiledValue;

public class ArgumentListener {
	public static CompiledValue convert(String toConvert, ElementType type) throws IllegalArgumentException{
		CompiledValue cv = new CompiledValue();
		if(type == ElementType.BOOLEAN){
			switch (toConvert){
			case "true":{
				cv.bool = Boolean.TRUE;
				return cv;
			}
			case "false":{
				cv.bool = Boolean.FALSE;
				return cv;
			}
			}
		} else if(type == ElementType.INTEGER){
			cv.set(new Integer(toConvert));
			return cv;
		} else {
			cv.set(toConvert);
			return cv;
		}
		return CompiledValue.nil;
	}
}
